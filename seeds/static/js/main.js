/**
 * Created by user on 11.10.2016.
 */
$(document).ready(function () {
   $('.slider').slick({
       slidesToShow: 4,
       slidesToScroll: 1,
       infinite: true
   });

    $('.fancybox').fancybox({
        autoResize: true,
        autoCenter: true,
        padding: 0,
        helpers : {
            overlay : {
                css : {
                    'background' : 'rgba(255, 255, 255, 0.8)'
                }
            }
        }
    });

    $("[data-class='fancybox']").fancybox({
        helpers: {
            overlay: {
              locked: false
            }
        }
    });
});