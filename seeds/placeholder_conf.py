CMS_PLACEHOLDER_CONF = {
    'phone': {
        'plugins': ['LinkPlugin'],
        'name': 'Телефон в шапке и подвале',
    },
    'address': {
        'plugins': ['TextPlugin'],
        'name': 'Адрес в шапке и подвале',
    },
    'mail': {
        'plugins': ['LinkPlugin'],
        'name': 'Электронная почта в шапке и подвале',
    },
    'feedback_form': {
        'plugins': ['FormPlugin'],
        'name': 'Форма обратной связи',
    },
    'order_form': {
      'name': 'Форма заказа',
    },
    'text': {
        'plugins': ['TextPlugin', 'LinkPlugin', 'PicturePlugin'],
        'name': 'Контент',
    },
    'seeds_card_top_left': {
        'name': 'Карточка продукта',
    },
    'seeds_card_top_right': {
        'name': 'Карточка продукта',
    },
    'seeds_card_bot_left': {
        'name': 'Карточка продукта',
    },
    'seeds_card_bot_right': {
        'name': 'Карточка продукта',
    },
}